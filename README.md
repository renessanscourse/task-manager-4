# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/i/hg0rN2ZtRIkL1A

https://yadi.sk/i/dnR3yzPhlpibCA

https://yadi.sk/i/6juGym9FXsMOgQ

https://yadi.sk/i/SUyI8yS2F5kzsw
